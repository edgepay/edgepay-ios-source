//
//  EPApplePayClient.swift
//  EdgePaySDK
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import Foundation
import PassKit

open class EPApplePayClient {
    
    // MARK: Properties
    private let TAG = String(describing: EPApplePayClient.self)
    
    private var apiClient: EPAPIClient
    
    // MARK: - Lifecycle
    
    /// Creates a ApplePayClient with specified apiClient.
    ///
    /// - Parameters:
    ///   - apiClient:  Base class to communicate with server API.
    ///
    /// - Returns:      The EPApplePayClient object.
    public init(apiClient: EPAPIClient) {
        self.apiClient = apiClient
    }
    
    /// Sets time-sensitive key that is used to getToken.
    ///
    /// - Parameters:
    ///   - merchantAuthKey: The time-sensitive key.
    public func configureMerchantAuthKey(_ merchantAuthKey: String) {
        self.apiClient.configureMerchantAuthKey(merchantAuthKey)
    }
    
    /// Return PKPaymentRequest from client configuration if apple pay is available.
    ///
    /// - Parameters:
    ///   - completion: A closure that is invoked when the getPaymentRequest method has completed. The closure takes two arguments:
    ///                 the PKPaymentRequest (contains payment request information) and the error (contains details about the error during getting the token).
    public func getPaymentRequest(completion: @escaping ((PKPaymentRequest?, EPError?) -> Void)) {
        apiClient.retrieveConfiguration { [weak self]  (configuration, error) in
            guard let self = self else { return }
            if let error = error {
                completion(nil, error)
                return
            }
            guard let configuration = configuration else { return }
            self.checkApplePayEnabled(configuration) { (flag, error) in
                if (!flag && error != nil ) {
                    if let error = error {
                        completion(nil, error)
                    }
                } else {
                    let paymentRequest = PKPaymentRequest()
                    paymentRequest.countryCode = configuration.applePayCountryCode
                    paymentRequest.currencyCode = configuration.applePayCurrencyCode
                    paymentRequest.merchantIdentifier = configuration.applePayMerchantIdentifier
                    paymentRequest.supportedNetworks = self.mapPKPaymentNetwork(from: configuration.applePaySupportedNetworks)
                    completion(paymentRequest, nil)
                }
            }
        }
    }
    
    /// Receives token based on PKPayment details.
    ///
    /// - Parameters:
    ///   - card:       The code of error.
    ///   - completion: A closure that is invoked when the getToken method has completed. The closure takes two arguments:
    ///                 the token (contains the single-use token string) and the error (contains details about the error during getting the token).
    public func getToken(payment: PKPayment, completion: @escaping ((EPToken?, EPError?) -> Void)) {
        apiClient.retrieveConfiguration { [weak self]  (configuration, error) in
            guard let self = self else { return }
            if let error = error {
                completion(nil, error)
                return
            }
            guard let configuration = configuration else {
                completion(nil, EPErrorImpl(errorCode: 0, errorType: .SERVICE_INVOCATION_ERROR, description: Constants.ERROR_PARSE_INVOCATION_ERROR))
                return
            }
            self.checkApplePayEnabled(configuration) { (flag, error) in
                if (!flag && error != nil ) {
                    if let error = error {
                        completion(nil, error)
                    }
                } else {
                if (configuration.result != "A") {
                    let error = EPErrorImpl(errorCode: EPApplePayErrorType.unsupported.rawValue, errorType: EPErrorType.APPLEPAY_ERROR, description: Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
                    if let message = error.errorDescription {
                        Logger.e(tag: self.TAG, message: message)
                    }
                    completion(nil, error)
                    return
                }
                let params = self.parametersForPayment(token: payment.token)
                self.apiClient.retrieveApplePayToken(parameters: params, completion: completion)
                }
            }
        }
    }
    
    private func checkApplePayEnabled(_ configuration: EPClientConfiguration, completion: @escaping ((Bool, EPError?)->Void) )  {
            if !configuration.applePayEnabled {
                let error = EPErrorImpl(errorCode: EPApplePayErrorType.unsupported.rawValue, errorType: EPErrorType.APPLEPAY_ERROR, description: Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
                if let message = error.errorDescription {
                    Logger.e(tag: self.TAG, message: message)
                }
                completion(false, error)
            } else {
                completion(true, nil)
        }
    }
    
    private func mapPKPaymentNetwork(from strings: [String]) -> [PKPaymentNetwork] {
        strings.map { PKPaymentNetwork(rawValue: $0) }
    }
    
    private func parametersForPayment(token: PKPaymentToken) -> [String: Any] {
        let dataParams = self.dataParametersForApplePayData(token: token)
        let parameters: [String: Any] = ["type": "apple_pay",
                                         "data": dataParams]
        return parameters
    }
    
    private func dataParametersForApplePayData(token: PKPaymentToken) -> [String: Any] {
        let paymentData = self.getPaymentData(token: token)
        let paymentMethod = self.getPaymentMethod(token: token)
        #if targetEnvironment(simulator)
        let applePaymentToken: [String: Any] = ["paymentData": paymentData,
                                                "paymentMethod": paymentMethod,
                                                "transactionIdentifier": "3FE1D486D5347917C4969DA2A80CE09C98186DD9D18…."]
        #else
        let applePaymentToken: [String: Any] = ["paymentData": paymentData,
                                                "paymentMethod": paymentMethod,
                                                "transactionIdentifier": token.transactionIdentifier]
        #endif
        let parameters: [String: Any] = ["applePaymentToken": applePaymentToken as Any]
        return parameters
    }
    
    private func getPaymentData(token: PKPaymentToken) -> [String: Any] {
        #if targetEnvironment(simulator)
        let header = self.getPaymentDataHeader(header: [:])
        let params: [String: Any] = ["version": "EC_v1" as Any,
                                     "data": "mIxYrbdb1fIqmEcdGPsmUQ8Q/lGJjfe0vT……………….." as Any,
                                     "signature": "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBgl……….." as Any,
                                     "header": header]
        
        return params
        #else
        let data = try? JSONSerialization.jsonObject(with: token.paymentData, options: [])
        guard let receivedData = data as? [String: Any],
              let paymentDataheader = receivedData["header"] as? [String: Any] else {
            return [:]
        }
        let header = self.getPaymentDataHeader(header: paymentDataheader)
        let params: [String: Any] = ["version": receivedData["version"] as Any,
                                     "data": receivedData["data"] as Any,
                                     "signature": receivedData["signature"] as Any,
                                     "header": header]
        
        return params
        #endif
    }
    
    private func getPaymentDataHeader(header: [String: Any]) -> [String: Any] {
        #if targetEnvironment(simulator)
        return [
            "ephemeralPublicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQg…." as Any,
            "publicKeyHash": "wIcjAmSC+ioMB6DqDXySGxEZy+QUw7tDrExK+mlNPoQ=" as Any,
            "transactionId": "3fe1d486d5347917c4969da2a80ce09c98186dd9d…." as Any
        ]
        #else
        return [
            "ephemeralPublicKey": header["ephemeralPublicKey"],
            "publicKeyHash": header["publicKeyHash"],
            "transactionId": header["transactionId"]
        ].compactMapValues { $0 }
        #endif
    }
    
    private func getPaymentMethod(token: PKPaymentToken) -> [String: Any] {
        #if targetEnvironment(simulator)
        return [
            "type": 2,
            "displayName": "MasterCard 8221",
            "network": "MasterCard"
        ]
        #else
        return [
            "type": token.paymentMethod.type.rawValue,
            "displayName": token.paymentMethod.displayName as Any,
            "network": token.paymentMethod.network?.rawValue as Any
        ]
        #endif
    }
    
}

