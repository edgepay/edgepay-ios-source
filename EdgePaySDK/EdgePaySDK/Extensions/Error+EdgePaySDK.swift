//
//  Error+EdgePaySDK.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Custom EdgePaySDK error.
enum EdgePaySDKError: Error {
    case serverSideError(code: Int, message: String)
}

/// Error descriprion for custom EdgePaySDK error.
extension EdgePaySDKError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .serverSideError(_, let message): return message
        }
    }
}
