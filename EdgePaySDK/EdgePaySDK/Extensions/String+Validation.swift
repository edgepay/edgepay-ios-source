//
//  String+Validation.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to validate card fields.
extension String {
    
    func isDigitsOnly() -> Bool {
        let regexStr = "[0-9]+"
        return NSPredicate(format: "SELF MATCHES %@", regexStr).evaluate(with: self)
    }
    
    func isCardNumber() -> Bool {
        let regexStr = """
            ^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|
            2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|
            7[0-1][0-9]{13}|720[0-9]{12})|5[1-5][0-9]{14}|
            6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|
            (?:2131|1800|35\\d{3})\\d{11})$
            """
        return NSPredicate(format: "SELF MATCHES %@", regexStr).evaluate(with: self)
    }
    
    func isExpirationMonth() -> Bool {
        let regexStr = "0[1-9]|[1-9]|1[0-2]"
        return NSPredicate(format: "SELF MATCHES %@", regexStr).evaluate(with: self)
    }
    
    func isExpirationYear() -> Bool {
        let regexStr = "^[0-9]{2}|[0-9]{4}"
        return NSPredicate(format: "SELF MATCHES %@", regexStr).evaluate(with: self)
    }
    
    func isCvv() -> Bool {
        let regexStr = "\\d{3,4}"
        return NSPredicate(format: "SELF MATCHES %@", regexStr).evaluate(with: self)
    }
}
