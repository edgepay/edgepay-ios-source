//
//  String+Subscript.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}
