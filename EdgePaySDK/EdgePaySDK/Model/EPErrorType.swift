//
//  EPErrorType.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to classify the type of encountered an error.
///
/// - VALIDATION_FAILED: Input validation error.
///
/// - SERVICE_INVOCATION_ERROR:  Server invocation error.
///
/// - APPLEPAY_ERROR:  ApplePay error.
public enum EPErrorType {
    case VALIDATION_FAILED, SERVICE_INVOCATION_ERROR, APPLEPAY_ERROR
}
