//
//  EPApplePayErrorType.swift
//  EdgePaySDK
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import Foundation

/// Domain for Apple Pay errors..
///
public enum EPApplePayErrorType: Int {
    /// Unknown error.
    case unknown = 0
    
    /// Apple Pay is disabled in the EdgePay.
    case unsupported
    
    /// EdgePay SDK is integrated incorrectly.
    case integration
}
