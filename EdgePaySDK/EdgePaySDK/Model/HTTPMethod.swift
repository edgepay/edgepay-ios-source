//
//  HTTPMethods.swift
//  EdgePaySDK
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import Foundation

public enum HTTPMethod {
    case post
    case get
    
    var stringValue: String {
        switch self {
        case .post:
            return "POST"
        case .get:
            return "GET"
        }
    }
}
