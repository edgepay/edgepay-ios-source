//
//  CardValidationField.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to classify the card validation fields.
///
/// - CARD_NUMBER: Card number field.
///
/// - CARD_EXPIRATION_MONTH: Card expiration month field.
///
/// - CARD_EXPIRATION_YEAR: Card expiration year field.
///
/// - CVV: Card verification value field.
public enum CardValidationField {
    case CARD_NUMBER, CARD_EXPIRATION_MONTH, CARD_EXPIRATION_YEAR, CVV
}
