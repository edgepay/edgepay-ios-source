//
//  ValidationError.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent an error that receiving during input params validation.
public class ValidationError {
    
    // MARK: Properties
    
    /// The field that caused the error.
    public private(set) var field: CardValidationField
    
    /// The error message.
    public private(set) var message: String
    
    // MARK: - Lifecycle
    /// Creates a validation error.
    ///
    /// - Parameters:
    ///   - field:     The field that caused the error.
    ///   - message:   The error message.
    ///
    /// - Returns: The error object.
    public init(field: CardValidationField, message: String) {
        self.field = field
        self.message = message
    }
}
