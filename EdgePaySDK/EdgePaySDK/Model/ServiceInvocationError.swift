//
//  ServiceInvocationError.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent an error that receiving during communicate with the server.
public class ServiceInvocationError {
    
    // MARK: Properties
    
    /// The result.
    public var result: String?
    
    /// The response code.
    public var responseCode: String?
    
    /// The response message.
    public var responseMessage: String?
}


enum ServiceInvocationCode: String {
    case EP0999
    case EP0055
    case EP0012
    case EP0900
    case EP0054
    case EP0528
    case EP0082
    
    var errorMessage: String {
        switch self {
        case .EP0999:
            return "Request a new Auth Key and resend request"
        case .EP0055:
            return "Display result to user and request corrected Date. Note: Requires a new Auth Key"
        case .EP0012:
            return "The Auth Key was invalid. Request another Auth key and resubmit"
        case .EP0900:
            return "This is an internal Key error. Call for help!"
        case .EP0054:
            return "The customer input an invalid card. Get a new key and prompt to retry card input"
        case .EP0528:
            return "Invalid Merchant ID"
        case .EP0082:
            return "Token Service Error Occured"
        }
    }
}
