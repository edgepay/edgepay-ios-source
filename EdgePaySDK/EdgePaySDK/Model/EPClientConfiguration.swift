//
//  EPClientConfiguration.swift
//  EdgePaySDK
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import Foundation

public class EPClientConfiguration: Decodable {
    
    public private(set) var result: String
    public private(set) var responseCode: String
    public private(set) var responseMessage: String
    
    public private(set) var applePayEnabled: Bool
    public private(set) var applePayMerchantIdentifier: String
    public private(set) var applePayCountryCode: String
    public private(set) var applePayCurrencyCode: String
    public private(set) var applePaySupportedNetworks: [String]
    
}
