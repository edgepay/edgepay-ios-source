//
//  EPCard.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent card details
public class EPCard {
    
    // MARK: Properties
    
    /// The card number.
    public private(set) var number: String
    
    /// The card expiration month.
    public private(set) var expirationMonth: String
    
    /// The card expiration year.
    public private(set) var expirationYear: String
    
    /// The card verification value(cvv).
    public private(set) var cvv: String?
    
    // MARK: - Lifecycle
    /// Creates a card.
    ///
    /// - Parameters:
    ///   - number:             The card number.
    ///   - expirationMonth:    The card expiration month.
    ///   - expirationYear:     The card expiration month.
    ///   - cvv:                The card verification value(cvv).
    ///
    /// - Returns: The card object.
    public init(number: String, expirationMonth: String, expirationYear: String, cvv: String?) {
        self.number = number
        self.expirationMonth = expirationMonth
        self.expirationYear = expirationYear
        self.cvv = cvv
    }
}
