//
//  EPErrorImpl.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Represents EPError implementation.
public class EPErrorImpl: EPError {
    
    // MARK: Properties
    
    /// The error code.
    public private(set) var errorCode: Int
    
    /// The type of error.
    public private(set) var errorType: EPErrorType
    
    /// The description of error.
    public private(set) var errorDescription: String?
    
    /// The array of validaion errors.
    public private(set) var validationErrors: [ValidationError]?
    
    /// The type of error.
    public private(set) var serviceInvocationError: ServiceInvocationError?
    
    static var emptyMerchantKeyError: EPErrorImpl {
        return EPErrorImpl(errorCode: NSURLErrorDataNotAllowed, errorType: .VALIDATION_FAILED, description: Constants.ERROR_MERCHANT_CANT_BE_EMPTY)
    }
    
    // MARK: - Lifecycle
    
    /// Creates a error with array of validation errors.
    ///
    /// - Parameters:
    ///   - errorCode:     The code of error.
    ///   - errorType:     The type of error.
    ///   - serviceInvocationError: The service invocation error.
    ///
    /// - Returns: The error object.
    public init(errorCode: Int, errorType: EPErrorType, validationErrors: [ValidationError]) {
        self.errorCode = errorCode
        self.errorType = errorType
        self.validationErrors = validationErrors
        
    }
    
    /// Creates a error with service invocation error.
    ///
    /// - Parameters:
    ///   - errorCode:     The code of error.
    ///   - errorType:     The type of error.
    ///   - serviceInvocationError: The service invocation error.
    ///
    /// - Returns: The error object.
    public init(errorCode: Int, errorType: EPErrorType, serviceInvocationError: ServiceInvocationError) {
        self.errorCode = errorCode
        self.errorType = errorType
        self.serviceInvocationError = serviceInvocationError
    }
    
    public init(errorCode: Int, errorType: EPErrorType, description: String) {
        self.errorCode = errorCode
        self.errorType = errorType
        self.errorDescription = description
    }
}
