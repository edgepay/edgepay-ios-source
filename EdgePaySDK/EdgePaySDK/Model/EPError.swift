//
//  EPError.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent interface to process the error codes and messages.
public protocol EPError {
    
    // MARK: Properties
    
    /// The error code.
    var errorCode: Int { get }
    
    /// The type of error.
    var errorType: EPErrorType { get }
    
    /// The description of error.
    var errorDescription: String? { get }
    
    /// The array of validaion errors.
    var validationErrors: [ValidationError]? { get }
    
    /// The server invocation error.
    var serviceInvocationError: ServiceInvocationError? { get }
}
