//
//  EPToken.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Contains the single-use token id.
public class EPToken {
    
    // MARK: Properties
    
    /// The tokenID.
    public private(set) var tokenId: String
    
    // MARK: - Lifecycle
    
    /// Creates a token.
    ///
    /// - Parameters:
    ///   - tokenId:     The tokenID.
    ///
    /// - Returns: The error object.
    public init(tokenId: String) {
        self.tokenId = tokenId
    }
}
