//
//  EPAPIClient.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Class acts as the entry point for accessing the EdgePay APIs.
open class EPAPIClient {
    
    // MARK: Properties
    
    private let TAG = String(describing: EPAPIClient.self)
    private let BASE_URL_PROD = "https://edgepayapi.com/"
    private let BASE_URL_TEST = "https://api.edgepay-uat.com/"
    private let GET_TOKEN = "tokenSU"
    private let CLIENTCONFIGURATION = "clientConfiguration"
    private let TOKEN = "pivotToken" 
    
    var manager: HttpRequestManagerProtocol    // Use internal  for unit tests.
    
    /// Generates unique externalReferenceID within a 3-day period.
    ///
    /// - Returns: The 16 digits unique value.
    private var externalReferenceId: UInt64 {
        let endDigits = UInt64(arc4random_uniform(901)) + 100
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        let timestamp = UInt64(since1970 * 1000)
        return timestamp * 1000 + UInt64(endDigits)
    }
    
    private var baseUrl: String
    private var merchantAuthKey: String = ""
    
    // MARK: - Lifecycle

    /// Initializes class for communicate with EdgePay APIs in prod mode.
    ///
    /// - Returns:      The EdgePaySDK object.
    public init() {
        let session = URLSession.shared
        manager = HttpRequestManager(session: session)
        baseUrl = BASE_URL_PROD
    }

    /// Initializes class for communicate with EdgePay APIs in test mode or prod.
    ///
    /// - Parameters:
    ///   - testMode:   The mode of SDK usage.
    ///
    /// - Returns:      The EdgePaySDK object.
    public init(testMode: Bool) {
        let session = URLSession.shared
        manager = HttpRequestManager(session: session)
        guard testMode else {
            baseUrl = BASE_URL_PROD
            return
        }
        baseUrl = BASE_URL_TEST
    }
      
    /// Sets time-sensitive key that is used to getToken.
    ///
    /// - Parameters:
    ///   - merchantAuthKey: The time-sensitive key.
    public func configureMerchantAuthKey(_ merchantAuthKey: String) {
        self.merchantAuthKey = merchantAuthKey
    }
    
    /// Calls the EdgePay API to tokenize the credit card details.
    ///
    /// - Parameters:
    ///   - card:       The validated card.
    ///   - completion: A closure that is invoked when the retrieveToken method has completed. The closure takes two arguments:
    ///                 the token (contains the single-use token string) and the error (contains details about the error during getting the token).
    ///
    /// - Returns:      A download file destination closure.
    func retrieveToken(card: EPCard, completion: @escaping (EPToken?, EPError?) -> Void) {
        if merchantAuthKey.isEmpty {
            completion(nil, EPErrorImpl.emptyMerchantKeyError)
        }
        let url = baseUrl + GET_TOKEN
        let parameters = createRequestParameters(card: card)
        let headers = createRequestHeaders(for: merchantAuthKey)
        Logger.d(tag: TAG, message: "GetToken request: \nURL: \(url) \nRequest: \(createTokenRequestLog(card: card))")
        processGetTokenRequest(url: url, parameters: parameters, headers: headers, completion: completion)
    }
    
    /// Calls the API to get the merchant configuration
    ///
    /// - Parameters:
    ///   - completion: A closure that is invoked when the retrieveConfiguration method has completed. The closure takes two arguments:
    ///                 the client configuration (contains the information about use apple pay configuration) and the error (contains details about the error during getting the configuration).
    ///
    /// - Returns:      A download file destination closure.
    func retrieveConfiguration(completion: @escaping ((EPClientConfiguration?, EPError?) -> Void)) {
        if merchantAuthKey.isEmpty {
            completion(nil, EPErrorImpl.emptyMerchantKeyError)
        }
        let url = baseUrl + CLIENTCONFIGURATION
        let headers = createEPApplePayRequestHeaders(for: merchantAuthKey)
        Logger.d(tag: TAG, message: "RetrieveConfiguration request: \nURL: \(url)")
        processGetConfigurationRequest(url: url, headers: headers, completion: completion)
    }
    
    /// Calls the EdgePay API to tokenize the the Apple Pay Token.
    ///
    /// - Parameters:
    ///   - params:     The payment parameters.
    ///   - completion: A closure that is invoked when the retrieveApplePayToken method has completed. The closure takes two arguments:
    ///                 the token (contains the single-use token string) and the error (contains details about the error during getting the token).
    ///
    /// - Returns:      A download file destination closure.
    func retrieveApplePayToken(parameters: [String : Any], completion: @escaping ((EPToken?, EPError?) -> Void)) {
        if merchantAuthKey.isEmpty {
            completion(nil, EPErrorImpl.emptyMerchantKeyError)
        }
        let url = baseUrl + TOKEN
        let headers = createEPApplePayRequestHeaders(for: merchantAuthKey)
        Logger.d(tag: TAG, message: "RetrieveApplePayToken request: \nURL: \(url)")
        processGetApplePayTokenRequest(url: url, parameters: parameters, headers: headers, completion: completion)
    }
    
    private func createRequestHeaders(for merchantAuthKey: String) -> [String: String] {
        let headers: [String: String] = ["Content-Type": "application/json",
                                         "cache-control": "no_cache",
                                         "externalReferenceID": "\(externalReferenceId)",
                                         "pivotAuthKey": merchantAuthKey]
        return headers
    }
    
    private func createRequestParameters(card: EPCard) -> [String: String] {
        let expirationDate = createExpirationDate(month: card.expirationMonth, year: card.expirationYear)
        var parameters: [String: String] = ["cardNumber": card.number,
                                            "cardExpirationDate": expirationDate]
        if let cvv = card.cvv {
            parameters["cvv2"] = cvv
        }
        return parameters
    }
        
    private func createEPApplePayRequestHeaders(for merchantAuthKey: String) -> [String: String] {
        let headers: [String: String] = ["Content-Type": "application/json",
                                         "cache-control": "no_cache",
                                         "authKey": merchantAuthKey]
        return headers
    }
       
    private func createTokenRequestLog(card: EPCard) -> [String: String] {
        let expirationDate = createExpirationDate(month: card.expirationMonth, year: card.expirationYear)
        var parameters: [String: String] = ["cardNumber": createMaskedCardNumber(card.number),
                                            "cardExpirationDate": expirationDate]
        if card.cvv != nil {
            parameters["cvv2"] = "***"
        }
        return parameters
    }
    
    private func createMaskedCardNumber(_ number: String) -> String {
        var result = ""
        for i in 0..<number.count {
            if i > 5 && i < number.count - 4 {
                result.append("*")
            } else {
                result.append(number[i])
            }
        }
        return result
    }
    
    /// Formats month and year fields to required server format.
    ///
    /// - Parameters:
    ///   - month: The card expiration month.
    ///   - year:  The card expiration year.
    ///
    /// - Returns: Formatted expiration date string.
    private func createExpirationDate(month: String, year: String) -> String {
        let formattedYear = String(year.suffix(2))
        return String(format: "%02d\(formattedYear)", Int(month) ?? 0)
    }
    
    private func processGetTokenRequest(url: String, parameters: [String: String],
                                        headers: [String: String], completion: @escaping ((EPToken?, EPError?) -> Void)) {
        manager.performWebServiceCall(url: url, httpMethod: .post,
                                      parameters: parameters, headers: headers) { [weak self] (data, response, error) in
                                        guard let strongSelf = self else { return }
                                        if let error = error {
                                            Logger.e(tag: strongSelf.TAG, e: error)
                                            let statusCode = (error as NSError).code
                                            let message = error.localizedDescription
                                            let epError = strongSelf.createCustomServiceInvocationError(with: statusCode, description: message)
                                            completion(nil, epError)
                                            return
                                        }
                                        guard let responseData = data else {
                                            let message = Constants.ERROR_EMPTY_RESPONSE_DATA
                                            Logger.e(tag: strongSelf.TAG, message: message)
                                            let error = strongSelf.createCustomServiceInvocationError(description: message)
                                            completion(nil, error)
                                            return
                                        }
                                        guard let response = response as? HTTPURLResponse, (200 ... 299) ~= response.statusCode else {
                                            let error = strongSelf.createServiceInvocationError(data: responseData)
                                            if let message = error.serviceInvocationError?.responseMessage {
                                                Logger.e(tag: strongSelf.TAG, message: message)
                                            }
                                            completion(nil, error)
                                            return
                                        }
                                        guard let token = ResponseParser.parseToken(responseData: responseData) else {
                                            let message = Constants.ERROR_CANT_PARSE_TOKEN
                                            Logger.e(tag: strongSelf.TAG, message: message)
                                            let error = strongSelf.createCustomServiceInvocationError(description: message)
                                            completion(nil, error)
                                            return
                                        }
                                        completion(token, nil)
        }
    }
    
    private func processGetConfigurationRequest(url: String,
                                                headers: [String: String], completion: @escaping ((EPClientConfiguration?, EPError?) -> Void)) {
        manager.performWebServiceCall(url: url, httpMethod: .get, parameters: [:], headers: headers) {  [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            if let error = error {
                Logger.e(tag: strongSelf.TAG, e: error)
                let statusCode = (error as NSError).code
                let message = error.localizedDescription
                let epError = strongSelf.createCustomServiceInvocationError(with: statusCode, description: message)
                completion(nil, epError)
                return
            }
            guard let responseData = data else {
                let message = Constants.ERROR_EMPTY_RESPONSE_DATA
                Logger.e(tag: strongSelf.TAG, message: message)
                let error = strongSelf.createCustomServiceInvocationError(description: message)
                completion(nil, error)
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ... 299) ~= response.statusCode else {
                let error = strongSelf.createServiceInvocationError(data: responseData)
                if let error = error.serviceInvocationError, let code = error.responseCode {
                    if let invocationCode = ServiceInvocationCode(rawValue: code) {
                        Logger.e(tag: strongSelf.TAG, message: invocationCode.errorMessage)
                    } else {
                        if let message = error.responseMessage {
                            Logger.e(tag: strongSelf.TAG, message:  message)
                        }
                    }
                }
                completion(nil, error)
                return
            }
            
            let parsedResponse: (configuration: EPClientConfiguration?, error: ServiceInvocationError?) = ResponseParser.parseClientConfiguration(responseData: responseData)
            guard let configuration = parsedResponse.configuration else {
                guard let error = parsedResponse.error else { return }
                let epError = EPErrorImpl.init(errorCode: NSURLErrorDataNotAllowed, errorType: .SERVICE_INVOCATION_ERROR, serviceInvocationError: error)
                    completion(nil, epError)
                    return
                }
            completion(configuration, nil)
        }
    }
    
    private func processGetApplePayTokenRequest(url: String, parameters: [String: Any],
                                                headers: [String: String], completion: @escaping ((EPToken?, EPError?) -> Void)) {
        manager.performWebServiceCall(url: url, httpMethod: .post,
                                      parameters: parameters, headers: headers) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            if let error = error {
                Logger.e(tag: strongSelf.TAG, e: error)
                let statusCode = (error as NSError).code
                let message = error.localizedDescription
                let epError = strongSelf.createCustomServiceInvocationError(with: statusCode, description: message)
                completion(nil, epError)
                return
            }
            guard let responseData = data else {
                let message = Constants.ERROR_EMPTY_RESPONSE_DATA
                Logger.e(tag: strongSelf.TAG, message: message)
                let error = strongSelf.createCustomServiceInvocationError(description: message)
                completion(nil, error)
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ... 299) ~= response.statusCode else {
                let error = strongSelf.createServiceInvocationError(data: responseData)
                if let error = error.serviceInvocationError, let code = error.responseCode {
                    if let invocationCode = ServiceInvocationCode(rawValue: code) {
                        Logger.e(tag: strongSelf.TAG, message: invocationCode.errorMessage)
                    } else {
                        if let message = error.responseMessage {
                            Logger.e(tag: strongSelf.TAG, message:  message)
                        }
                    }
                }
                completion(nil, error)
                return
            }
            guard let token = ResponseParser.parseToken(responseData: responseData) else {
                let message = Constants.ERROR_CANT_PARSE_TOKEN
                Logger.e(tag: strongSelf.TAG, message: message)
                let error = strongSelf.createCustomServiceInvocationError(description: message)
                completion(nil, error)
                return
            }
            completion(token, nil)
        }
    }
    
    /// Creates service invocation error due to connectivity issue.
    ///
    /// - Parameters:
    ///   - description: The error message.
    ///
    /// - Returns: The service invocation error.
    private func createCustomServiceInvocationError(with code: Int = NSURLErrorDataNotAllowed, description: String) -> EPError {
        let error = ServiceInvocationError()
        error.responseMessage = description
        return EPErrorImpl(errorCode: code, errorType: EPErrorType.SERVICE_INVOCATION_ERROR, serviceInvocationError: error)
    }
    
    /// Creates service invocation error due to connectivity issue.
    ///
    /// - Parameters:
    ///   - data: The data with error information.
    ///
    /// - Returns: The service invocation error.
    private func createServiceInvocationError(data: Data) -> EPError {
        let error = ResponseParser.parseServiceInvocationError(responseData: data)
        return EPErrorImpl(errorCode: NSURLErrorDataNotAllowed,
                           errorType: EPErrorType.SERVICE_INVOCATION_ERROR, serviceInvocationError: error)
    }
}
