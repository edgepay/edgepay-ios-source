//
//  Logger.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent class for logging in a debug.
class Logger {
    private init() {}
    
    // MARK: Properties
    
    private static var enableLogging = false
    
    static func d(tag: String, message: String) {
        if enableLogging {
            print("\(tag) \(message)")
        }
    }
    
    static func e(tag: String, message: String) {
        if enableLogging {
            print("\(tag) \(message)")
        }
    }
    
    static func e(tag: String, e: Error) {
        if enableLogging {
            print("\(tag) \(e.localizedDescription)")
        }
    }
}
