//
//  ResponseParser.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to parse response data to the token or the error, which occurs during getting the token.
class ResponseParser {
    
    private static let TAG = String(describing: ResponseParser.self)
    
    static func parseToken(responseData: Data) -> EPToken? {
        var token: EPToken?
        let data = try? JSONSerialization.jsonObject(with: responseData, options: [])
        guard let receivedData = data as? [String: Any],
              let tokenID = receivedData["tokenID"] as? String else {
            return token
        }
        token = EPToken(tokenId: tokenID)
        return token
    }
    
    static func parseClientConfiguration(responseData: Data) -> (configuration: EPClientConfiguration?, error: ServiceInvocationError?) {
        let error = ServiceInvocationError()
        let data = try? JSONSerialization.jsonObject(with: responseData, options: [.allowFragments])
        guard let receivedData = data as? [String: Any] else {
            error.responseMessage = Constants.ERROR_PARSE_INVOCATION_ERROR
            return (nil, error)
        }
        if receivedData["applePayMerchantIdentifier"] == nil {
            let invocationError = self.parseServiceInvocationError(responseData: responseData)
            invocationError.responseMessage = Constants.ERROR_APPLEPAY_IS_NOT_ENABLED
            return (nil, invocationError)
        } else {
            let configuration = try? JSONDecoder().decode(EPClientConfiguration.self, from: responseData)
            return (configuration, nil)
        }
    }
    
    static func parseServiceInvocationError(responseData: Data) -> ServiceInvocationError {
        let error = ServiceInvocationError()
        let data = try? JSONSerialization.jsonObject(with: responseData, options: [])
        guard let receivedData = data as? [String: Any] else {
            error.responseMessage = Constants.ERROR_PARSE_INVOCATION_ERROR
            return error
        }
        error.result = receivedData["result"] as? String
        error.responseCode = receivedData["responseCode"] as? String ?? receivedData["status"] as? String
        if let responseCode = error.responseCode, let serviceCode = ServiceInvocationCode.init(rawValue: responseCode) {
            error.responseMessage = serviceCode.errorMessage
        } else {
            error.responseMessage = receivedData["responseMessage"] as? String ?? receivedData["error"] as? String ?? receivedData["message"] as? String
        }
        return error
    }
}
