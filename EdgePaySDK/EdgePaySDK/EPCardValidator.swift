//
//  EPCardValidator.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent card details validation.
final class EPCardValidator {
    private init() {}
    
    static func validate(card: EPCard) -> [ValidationError] {
        var errors: [ValidationError] = []
        if let errorText = validateCardNumber(card.number) {
            errors.append(ValidationError(field: CardValidationField.CARD_NUMBER, message: errorText))
        }
        if let errorText = validateExpirationMonth(card.expirationMonth) {
            errors.append(ValidationError(field: CardValidationField.CARD_EXPIRATION_MONTH, message: errorText))
        }
        if let errorText = validateExpirationYear(card.expirationYear) {
            errors.append(ValidationError(field: CardValidationField.CARD_EXPIRATION_YEAR, message: errorText))
        }
        if let errorText = validateCvv(card.cvv) {
            errors.append(ValidationError(field: CardValidationField.CVV, message: errorText))
        }
        return errors
    }
    
    private static func validateCardNumber(_ cardNumber: String?) -> String? {
        guard let value = cardNumber, !value.isEmpty else {
            return Constants.ERROR_CARD_NUMBER_REQUIRED
        }
        if !value.isDigitsOnly() {
            return Constants.ERROR_MUST_BE_NUMBER
        }
        if !value.isCardNumber() {
            return Constants.ERROR_INVALID_INPUT
        }
        return nil
    }
    
    private static func validateExpirationMonth(_ expirationMonth: String?) -> String? {
        guard let value = expirationMonth, !value.isEmpty else {
            return Constants.ERROR_EXPIRY_MONTH_REQUIRED
        }
        if !value.isExpirationMonth() {
            return Constants.ERROR_EXPIRY_MONTH_RESTRICTION
        }
        return nil
    }
    
    private static func validateExpirationYear(_ expirationYear: String?) -> String? {
        guard let value = expirationYear, !value.isEmpty else {
            return Constants.ERROR_EXPIRY_YEAR_REQUIRED
        }
        if !value.isExpirationYear() {
            return Constants.ERROR_EXPIRY_YEAR_RESTRICTION
        }
        return nil
    }
    
    private static func validateCvv(_ cvv: String?) -> String? {
        guard let value = cvv, !value.isEmpty else {
            return nil
        }
        guard value.isCvv() else {
            return Constants.ERROR_CVV_RESTRICTION
        }
        return nil
    }
}
