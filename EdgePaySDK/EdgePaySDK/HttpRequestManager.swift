//
//  HttpRequestManager.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// A type that can perform HTTP request.
protocol HttpRequestManagerProtocol {
    typealias completion = ( _ data: Data?, _ response: URLResponse? , _ error: Error?) -> Void

    func performWebServiceCall(url: String, httpMethod: HTTPMethod, parameters: [String: Any],
                               headers: [String: String], completion: @escaping completion)
}

/// Used to perform http requests.
class HttpRequestManager: HttpRequestManagerProtocol {
    
    // MARK: Properties
    
    private let session: URLSessionProtocol

    init(session: URLSessionProtocol) {
        self.session = session
    }
    
    func performWebServiceCall(url: String, httpMethod: HTTPMethod, parameters: [String: Any],
                               headers: [String: String], completion: @escaping HttpRequestManagerProtocol.completion) {
        guard let url = URL(string: url) else {
            let error = EdgePaySDKError.serverSideError(code: NSURLErrorBadURL, message: Constants.ERROR_BAD_SERVER_URL)
            completion(nil, nil, error)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.stringValue
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
        if httpMethod != HTTPMethod.get {
            do {
                let bodyData = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = bodyData
            } catch let error {
                let error = EdgePaySDKError.serverSideError(code: NSURLErrorDataNotAllowed, message: error.localizedDescription)
                completion(nil, nil, error)
                return
            }
        }
        let task = session.dataTask(with: request) { (data, response, error) in
            completion(data, response, error)
        }
        task.resume()
    }
}
