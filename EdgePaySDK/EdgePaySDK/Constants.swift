//
//  Constants.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Class with error messages.
final class Constants {
    private init(){}
    
    // MARK: Properties
    
    static let ERROR_INVALID_INPUT = "Invalid Input."
    static let ERROR_MUST_BE_NUMBER = "Must be a number."
    static let ERROR_CARD_NUMBER_REQUIRED = "Card number is required."
    static let ERROR_EXPIRY_MONTH_REQUIRED = "Expiration month is required."
    static let ERROR_EXPIRY_MONTH_RESTRICTION = "Expiration month must be from 01 to 12 or from 1 to 12."
    static let ERROR_EXPIRY_YEAR_REQUIRED = "Expiration year is required."
    static let ERROR_EXPIRY_YEAR_RESTRICTION = "Expiration year must be 2 or 4 digits."
    static let ERROR_CVV_RESTRICTION = "Cvv length must be 3 or 4 digits."
    static let ERROR_BAD_SERVER_URL = "Bad server url."
    static let ERROR_EMPTY_RESPONSE_DATA = "Server response data is empty."
    static let ERROR_APPLEPAY_IS_NOT_ENABLED = "Apple Pay is not enabled. Please ensure that you have a valid Apple Pay Certificate in the merchant portal."
    static let ERROR_EMPTY_MERCHANT_AUTH_KEY = "Merchant auth key is empty."
    static let ERROR_MERCHANT_CANT_BE_EMPTY = "Merchant auth key can't be empty"
    static let ERROR_PARSE_INVOCATION_ERROR = "Could not parse the response with an error!"
    static let ERROR_CANT_PARSE_TOKEN = "Could not parse token!"
    static let ERROR_CANT_PARSE_CLIENT_CONFIGURATION = "Could not parse client configuration!"
    static let ERROR_APICLIENT_IS_NILL = "EPAPIClient is nil."
}
