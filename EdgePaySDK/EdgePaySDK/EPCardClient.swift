//
//  EPCardClient.swift
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

/// Used to represent class for work with token API.
open class EPCardClient {
    
    // MARK: Properties
    
    private var apiClient: EPAPIClient
    
    // MARK: - Lifecycle
    
    /// Creates a cardClient with specified apiClient.
    ///
    /// - Parameters:
    ///   - apiClient:  Base class to communicate with server API.
    ///
    /// - Returns:      The EPCardClient object.
    public init(apiClient: EPAPIClient) {
        self.apiClient = apiClient
    }
    
    /// Receives token based on card details.
    ///
    /// - Parameters:
    ///   - card:       The code of error.
    ///   - completion: A closure that is invoked when the getToken method has completed. The closure takes two arguments:
    ///                 the token (contains the single-use token string) and the error (contains details about the error during getting the token).
    public func getToken(card: EPCard, completion: @escaping ((EPToken?, EPError?) -> Void)) {
        let validationErrors = EPCardValidator.validate(card: card)
        guard validationErrors.count == 0 else {
            completion(nil, prepareValidationErrors(validationErrors))
            return
        }
        apiClient.retrieveToken(card: card, completion: completion)
    }
    
    private func prepareValidationErrors(_ errors: [ValidationError]) -> EPError {
        return EPErrorImpl(errorCode: NSURLErrorDataNotAllowed, errorType: EPErrorType.VALIDATION_FAILED, validationErrors: errors)
    }
}
