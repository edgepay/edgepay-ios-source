//
//  EdgePaySDK.h
//  EdgePaySDK
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EdgePaySDK.
FOUNDATION_EXPORT double EdgePaySDKVersionNumber;

//! Project version string for EdgePaySDK.
FOUNDATION_EXPORT const unsigned char EdgePaySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EdgePaySDK/PublicHeader.h>
