//
//  ResponseParserTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import XCTest
@testable import EdgePaySDK

class ResponseParserTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testParseTokenExist() {
        let responseString = """
            {
                "result": "A",
                "tokenID": "5322170499520100"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let token = ResponseParser.parseToken(responseData: responseData!)
        XCTAssertNotNil(token)
        XCTAssertEqual("5322170499520100", token?.tokenId)
    }
    
    func testParseTokenIsNotValid() {
        let responseString = """
            {
                "result": "A"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let token = ResponseParser.parseToken(responseData: responseData!)
        XCTAssertNil(token)
    }
    
    func testParseServiceInvocationError() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0055",
                "responseMessage": "Invalid Expiry  Date (MMYY)"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }
    
    func testParseServiceInvocationErrorIsNotValid() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0055",
                "Invalid Expiry  Date (MMYY)"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNil(error.result)
    }
    
    func testParseServiceInvocationErrorRequesANewAuthKey() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0999",
                "responseMessage": "Request a new Auth Key and resend request"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }
    
    func testParseServiceInvocationErrorAuthKeyInvalid() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0012",
                "responseMessage": "The Auth Key was invalid. Request another Auth key and resubmit"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }
    
    func testParseServiceInvocationInternalKeyError() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0900",
                "responseMessage": "This is an internal Key error. Call for help!"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }

    func testParseServiceInvocationErrorCustomerInputInvalidCard() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0054",
                "responseMessage": "The customer input an invalid card. Get a new key and prompt to retry card input"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }
    
    func testParseServiceInvocationErrorCardExpirationDate() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0900",
                "responseMessage": "This is an internal Key error. Call for help!"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }
    
    func testParseClientConfiguration() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0900",
                "responseMessage": "This is an internal Key error. Call for help!"
            }
            """
        let expectation = XCTestExpectation(description: "Parse client configuration")
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseClientConfiguration(responseData: responseData!)
        
        XCTAssertNotNil(error.error)
        if let error = error.error  {
            XCTAssertEqual(Constants.ERROR_APPLEPAY_IS_NOT_ENABLED, error.responseMessage)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testParseClientConfigurationErrorCustomerInput() {
        let responseString = """
            {
                "result": "D",
                "responseCode": "EP0528",
                "responseMessage": "The customer input an invalid card. Get a new key and prompt to retry card input"
            }
            """
        let responseData = responseString.data(using: .utf8)
        XCTAssertNotNil(responseData)
        guard responseData != nil else { return }
        let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
        XCTAssertNotNil(error)
    }
    
    func testParseServiceInvocationErrorWithError() {
        func testParseServiceInvocationError() {
            let responseString = """
                {
                    "result": "Error",
                    "status": "404",
                    "error": "Not Found"
                }
                """
            let responseData = responseString.data(using: .utf8)
            XCTAssertNotNil(responseData)
            guard responseData != nil else { return }
            let error = ResponseParser.parseServiceInvocationError(responseData: responseData!)
            XCTAssertNotNil(error)
        }
    }

    func testPerformanceExample() {
        self.measure {
        }
    }

}
