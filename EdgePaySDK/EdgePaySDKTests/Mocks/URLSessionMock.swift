//
//  URLSessionMock.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation
@testable import EdgePaySDK

class URLSessionMock: URLSessionProtocol {
    
    var dataTask = URLSessionDataTaskMock()
    var data: Data?
    var error: Error?
    
    private (set) var lastURL: URL?
    
    private func successHttpURLResponse(request: URLRequest) -> URLResponse {
        return HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
    }
    
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        lastURL = request.url
        
        completionHandler(data, successHttpURLResponse(request: request), error)
        return dataTask
    }
}

class URLSessionDataTaskMock: URLSessionDataTaskProtocol {
    private (set) var resumeWasCalled = false
    
    func resume() {
        resumeWasCalled = true
    }
}
