//
//  EPAPIClientMock.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation
@testable import EdgePaySDK

public class EPAPIClientMock: EPAPIClient {
    
    public override init(testMode: Bool) {
        super.init(testMode: testMode)
    }
    
    override public func retrieveToken(card: EPCard, completion: @escaping ((EPToken?, EPError?) -> Void)) {
        completion(nil, nil)
    }
}
