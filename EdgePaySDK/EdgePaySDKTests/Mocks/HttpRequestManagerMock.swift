//
//  HttpRequestManagerMock.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation
@testable import EdgePaySDK

enum ResponseType {
    case OK, ERROR(code: Int)
}

class HttpRequestManagerMock: HttpRequestManagerProtocol {
    
    var responseType = ResponseType.OK
    var data: Data?
    var error: Error?
    
    private func configureHttpURLResponse(url: String) -> URLResponse {
        var statusCode = 200
        if case ResponseType.ERROR(let code) = responseType {
            statusCode = code
        }
        let url = URL(string: url)!
        return HTTPURLResponse(url: url, statusCode: statusCode, httpVersion: "HTTP/1.1", headerFields: nil)!
    }
    
    func performWebServiceCall(url: String, httpMethod: HTTPMethod, parameters: [String : Any], headers: [String : String], completion: @escaping HttpRequestManagerProtocol.completion) {
        completion(data, configureHttpURLResponse(url: url), error)
    }
}
