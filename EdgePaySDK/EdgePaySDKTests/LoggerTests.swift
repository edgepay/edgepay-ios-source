//
//  LoggerTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import XCTest
@testable import EdgePaySDK

class LoggerTest: XCTestCase {
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testLoggerD() {
        
        Logger.d(tag: "LoggerTest", message: "tet logger d")
    }
    
    func testLoggerE() {
        let error = EdgePaySDKError.serverSideError(code: 01, message: "test logger E message")
        Logger.e(tag: "LoggerTest", e: error)
    }
    
    
    func testPerformanceExample() {
        self.measure {
        }
    }
}
