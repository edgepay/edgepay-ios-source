//
//  EPApplePayClientTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import XCTest
import PassKit
@testable import EdgePaySDK

class EPApplePayClientTests: XCTestCase {
    
    private var testApiClient = EPAPIClientMock(testMode: true)
    private var testApplePayClient: EPApplePayClient!
    
    private var prodApiClient = EPAPIClientMock(testMode: false)
    private var prodApplePayClient: EPApplePayClient!
    
    private let applePayMerchantAuthKey = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJtZXJjaGFudEtleVwiOlwiQVZLTnBvRDFsV"
    
    override func setUp() {
        super.setUp()
        
        testApplePayClient = EPApplePayClient(apiClient: testApiClient)
        prodApplePayClient = EPApplePayClient(apiClient: prodApiClient)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetPaymentRequestProd() {
        prodApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let responseString = """
        {
            "result": "A",
            "responseCode": "EP00XX",
            "responseMessage": "Successful",
            "applePayEnabled": true,
            "applePayMerchantIdentifier": "merchant.my-apple-identifier",
            "applePayCountryCode": "US",
            "applePayCurrencyCode": "USD",
            "applePaySupportedNetworks": [ "AmEx" ]
        }
        """
        
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        prodApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get payment request with enable apple pay")
        
        prodApplePayClient.getPaymentRequest { (request, error) in
            XCTAssertNotNil(request)
            XCTAssertNil(error)
            if let request = request {
                XCTAssertNotNil(request.countryCode)
                XCTAssertNotNil(request.currencyCode)
                XCTAssertNotNil(request.supportedNetworks)
                XCTAssertEqual(request.merchantIdentifier, "merchant.my-apple-identifier")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfigurationWithError() {
        testApiClient.configureMerchantAuthKey("1234")
        let payment = PKPayment()
        
        let manager = HttpRequestManagerMock()
        let errorMessage = "Error"
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get Configuration error")
        testApplePayClient.getToken(payment: payment) { (token, error) in
            XCTAssertNil(token)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, errorMessage)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testGetPaymentRequestTest() {
        testApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let responseString = """
           {
             "result": "A",
            "responseCode": "EP00XX",
            "responseMessage": "Successful",
            "applePayEnabled": true,
            "applePayMerchantIdentifier": "merchant.my-apple-identifier",
            "applePayCountryCode": "US",
            "applePayCurrencyCode": "USD",
            "applePaySupportedNetworks": [ "AmEx" ]
            }
        """
        
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get payment request with enable apple pay")
        
        testApplePayClient.getPaymentRequest { (request, error) in
            XCTAssertNotNil(request)
            XCTAssertNil(error)
            if let request = request {
                XCTAssertNotNil(request.countryCode)
                XCTAssertNotNil(request.currencyCode)
                XCTAssertNotNil(request.supportedNetworks)
                XCTAssertEqual(request.merchantIdentifier, "merchant.my-apple-identifier")
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testGetToken() {
        testApplePayClient.configureMerchantAuthKey("12345")
        let payment = PKPayment()
        
        let responseString = """
            {
                "result": "A",
                "responseCode": "EP00XX",
                "responseMessage": "Successful",
                "applePayEnabled": true,
                "applePayMerchantIdentifier": "merchant.my-apple-identifier",
                "applePayCountryCode": "US",
                "applePayCurrencyCode": "USD",
                "applePaySupportedNetworks": [ "AmEx" ],

                "merchantID": "9330585160",
                "tokenID": "5480036527610742",
                "cardExpirationDate": "0230"
            }
        """
        
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        testApiClient.manager = manager
        
        let expectation = XCTestExpectation(description: "Get token valid request")
        testApplePayClient.getToken(payment: payment) { (token, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(token)
            if let token = token {
                XCTAssertEqual("5480036527610742", token.tokenId)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testGetFailedToken() {
        testApplePayClient.configureMerchantAuthKey("jvhikhjnb23i234jjbjb2bn3423.23j4bj2lj3")
        let payment = PKPayment()
        
        let responseString = """
        {
            "result": "D",
            "responseCode": "EP0012",
            "responseMessage": "Authentication failed, please use valid credentials"
        }
        """
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token auth failed")
        testApplePayClient.getToken(payment: payment) { (token, error) in
            XCTAssertNotNil(error)
            XCTAssertNil(token)
            
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseCode, "EP0012")
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetPaymentResponseWithErrorTest() {
        testApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        let errorMessage = Constants.ERROR_EMPTY_RESPONSE_DATA
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        testApplePayClient.getPaymentRequest() { (paymentRequest, error) in
            XCTAssertNil(paymentRequest)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetPaymentResponseWithErrorProd() {
        prodApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        let errorMessage = Constants.ERROR_EMPTY_RESPONSE_DATA
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        prodApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        prodApplePayClient.getPaymentRequest() { (paymentRequest, error) in
            XCTAssertNil(paymentRequest)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetPaymentEmptyResponseTest() {
        testApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        testApplePayClient.getPaymentRequest() { (paymentRequest, error) in
            XCTAssertNil(paymentRequest)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetPaymentErrorResponseTest() {
        testApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        manager.responseType = .ERROR(code: 404)
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        testApplePayClient.getPaymentRequest() { (paymentRequest, error) in
            XCTAssertNil(paymentRequest)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetPaymentRequestWithDisabledApplePayeResponse() {
        let responseString = """
        {
            "result": "A",
            "responseCode": "EP00XX",
            "responseMessage": "Successful",
            "applePayEnabled": false,
            "applePayMerchantIdentifier": "merchant.my-apple-identifier",
            "applePayCountryCode": "US",
            "applePayCurrencyCode": "USD",
            "applePaySupportedNetworks": [ "AmEx" ]
        }
        """
        
        testApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        testApplePayClient.getPaymentRequest() { (paymentRequest, error) in
            XCTAssertNil(paymentRequest)
            XCTAssertNotNil(error)
            if let message = error?.errorDescription {
                XCTAssertEqual(message, Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetTokenRequestWithDisabledApplePayeResponse() {
        let payment = PKPayment()
        let responseString = """
        {
            "result": "A",
            "responseCode": "EP00XX",
            "responseMessage": "Successful",
            "applePayEnabled": false,
            "applePayMerchantIdentifier": "merchant.my-apple-identifier",
            "applePayCountryCode": "US",
            "applePayCurrencyCode": "USD",
            "applePaySupportedNetworks": [ "AmEx" ]
        }
        """
        
        testApplePayClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        testApiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        testApplePayClient.getToken(payment: payment) { (paymentRequest, error) in
            XCTAssertNil(paymentRequest)
            XCTAssertNotNil(error)
            if let message = error?.errorDescription {
                XCTAssertEqual(message, Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testPerformanceExample() {
        self.measure {
        }
    }
    
}
