//
//  EPAPIClientTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import XCTest
@testable import EdgePaySDK

class EPAPIClientTests: XCTestCase {
    
    private var card: EPCard!
    private let apiClient = EPAPIClient()
    private let applePayBodyParams = """
            {
                "type": "apple_pay",
                "data": {
                    "applePaymentToken": {
                        "paymentData": {
                            "version": "EC_v1",
                            "data": "mIxYrbdb1fIqmEcdGPsmUQ8Q/lGJjfe0vT………………..",
                            "signature": "MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBgl………..",
                            "header": {
                                "ephemeralPublicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQg….",
                                "publicKeyHash": "wIcjAmSC+ioMB6DqDXySGxEZy+QUw7tDrExK+mlNPoQ=",
                                "transactionId": "3fe1d486d5347917c4969da2a80ce09c98186dd9d…."
                            }
                        },
                        "paymentMethod": {
                            "type": 2,
                            "displayName": "MasterCard 8221",
                            "network": "MasterCard",
                            "billingAddress": null
                        },
                        "transactionIdentifier": "3FE1D486D5347917C4969DA2A80CE09C98186DD9D18…."
                    }
                }
            }
            """
    private let applePayMerchantAuthKey = " eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJtZXJjaGFudEtleVwiOlwiQVZLTnBvRDFsV"
    
    override func setUp() {
        super.setUp()
        
        card = EPCard(number: "4111111111111111", expirationMonth: "02", expirationYear: "2021", cvv: "12")
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetTokenWithoutAuthKey() {
        let expectation = XCTestExpectation(description: "Get token without Auth Key")
        apiClient.retrieveToken(card: card) { (token, error) in
            XCTAssertNotNil(error)
            XCTAssertNil(token)
            if let token = token {
                XCTAssertEqual("5322170499520100", token.tokenId)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetTokenValidRequest() {
        let responseString = """
            {
                "result": "A",
                "tokenID": "5322170499520100"
            }
            """
        let expectedData = responseString.data(using: .utf8)
        apiClient.configureMerchantAuthKey("1234")
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token valid request")
        apiClient.retrieveToken(card: card) { (token, error) in
            XCTAssertNotNil(token)
            XCTAssertNil(error)
            if let token = token {
                XCTAssertEqual("5322170499520100", token.tokenId)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetTokenInvalidRequest() {
        let responseString = """
            {
                "result": "A"
            }
            """
        let expectedData = responseString.data(using: .utf8)
        apiClient.configureMerchantAuthKey("1234")
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token invalid request")
        apiClient.retrieveToken(card: card) { (token, error) in
            XCTAssertNil(token)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_CANT_PARSE_TOKEN)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetTokenError() {
        apiClient.configureMerchantAuthKey("1234")
        let manager = HttpRequestManagerMock()
        let errorMessage = "Error"
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token error")
        apiClient.retrieveToken(card: card) { (token, error) in
            XCTAssertNil(token)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, errorMessage)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetTokenEmptyResponse() {
        apiClient.configureMerchantAuthKey("1234")
        let manager = HttpRequestManagerMock()
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token error")
        apiClient.retrieveToken(card: card) { (token, error) in
            XCTAssertNil(token)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testProcessGetTokenErrorResponse() {
        let responseString = """
            {
                "result": "D",
                "responseCode":"EP0055",
                "responseMessage": "Display result to user and request corrected Date. Note: Requires a new Auth Key"
            }
            """
        let errorData = responseString.data(using: .utf8)
        apiClient.configureMerchantAuthKey("1234")
        let manager = HttpRequestManagerMock()
        manager.responseType = ResponseType.ERROR(code: 500)
        manager.data = errorData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token error")
        apiClient.retrieveToken(card: card) { (token, error) in
            XCTAssertNil(token)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, "Display result to user and request corrected Date. Note: Requires a new Auth Key")
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfiguration() {
        apiClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        
        let responseString = """
            {
                "result": "A",
                "responseCode": "EP00XX",
                "responseMessage": "Successful",
                "applePayEnabled": true,
                "applePayMerchantIdentifier": "merchant.my-apple-identifier",
                "applePayCountryCode": "US",
                "applePayCurrencyCode": "USD",
                "applePaySupportedNetworks": [
                    "AmEx",
                    "Discover",
                    "MasterCard",
                    "Visa",
                    "JCB"
                ]
            }
        """
        
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token valid request")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNotNil(configuration)
            XCTAssertNil(error)
            if let configuration = configuration {
                XCTAssertEqual("merchant.my-apple-identifier", configuration.applePayMerchantIdentifier)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfigurationWithApplePayDisabled() {
        let authKey = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJtZXJjaGFudEtleVwiOlwiQVZLTnBvRDFsVm4rVkN4Uz"
        let responseString = """
            {
                "result": "A",
                "responseCode": "EP00XX",
                "responseMessage": "Successful",
                "applePayEnabled": false,
                "applePayMerchantIdentifier": "merchant.my-apple-identifier",
                "applePayCountryCode": "US",
                "applePayCurrencyCode": "USD",
                "applePaySupportedNetworks": [
                    "AmEx",
                    "Discover",
                    "MasterCard",
                    "Visa",
                    "JCB"
                ]
            }
        """
        apiClient.configureMerchantAuthKey(authKey)
        
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get token valid request")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNotNil(configuration)
            XCTAssertNil(error)
            if let configuration = configuration {
                XCTAssertEqual("merchant.my-apple-identifier", configuration.applePayMerchantIdentifier)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfigurationWithEmptyAuthKey() {
        let authKey = ""
        let responseString = """
            "responseMessage": "Merchant auth key can't be empty"
        """
        apiClient.configureMerchantAuthKey(authKey)
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get сonfiguration with empty authKey")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNil(configuration)
            XCTAssertNotNil(error)
            if let error = error {
                if error.errorDescription != nil {
                    XCTAssertNotNil(error.errorDescription)
                    XCTAssertEqual(error.errorDescription, Constants.ERROR_MERCHANT_CANT_BE_EMPTY)
                } else if let invocationError = error.serviceInvocationError {
                    XCTAssertNotNil(invocationError.responseMessage)
                    XCTAssertEqual(invocationError.responseMessage, "Could not parse the response with an error!")
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfigurationWithWrongAuthKey() {
        let authKey = "1MTYwXCIsXCJleHBpcmVzSW5TZWNvbmRz11111111133333333333"
        let responseString = """
        {
            "result": "R",
            "responseCode": "EP0999",
            "responseMessage": "System Error"
        }
        """
        apiClient.configureMerchantAuthKey(authKey)
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get сonfiguration request failed")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNil(configuration)
            XCTAssertNotNil(error)
            
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseCode, "EP0999")
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfigurationWithUnknownError() {
        let authKey = "1MTYwXCIsXCJleHBpcmVzSW5TZWNvbmRz11111111133333333333"
        let responseString = """
        {
            "result": "R",
            "responseCode": "EP007",
            "responseMessage": "New error cod that may not be known to the library"
        }
        """
        apiClient.configureMerchantAuthKey(authKey)
        let expectedData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get сonfiguration request failed")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNil(configuration)
            XCTAssertNotNil(error)
            
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseCode, "EP007")
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_APPLEPAY_IS_NOT_ENABLED)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testRetrieveConfigurationWithMerchantAuthKey() {
        let responseString = """
        {
            "result": "A",
            "tokenID": "5480036527610742",
        }
        """
        
        if let data = applePayBodyParams.data(using: .utf8)
        {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            {
                apiClient.configureMerchantAuthKey(applePayMerchantAuthKey)
                
                let expectedData = responseString.data(using: .utf8)
                let manager = HttpRequestManagerMock()
                manager.data = expectedData
                apiClient.manager = manager
                let expectation = XCTestExpectation(description: "Get token valid request")
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNotNil(token)
                    XCTAssertNil(error)
                    if let token = token {
                        XCTAssertEqual("5480036527610742", token.tokenId)
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    func testRetrieveConfigurationWithEmptyResponse() {
        apiClient.configureMerchantAuthKey("1sdfghhjkjhgvbc")
        let manager = HttpRequestManagerMock()
        let errorMessage = Constants.ERROR_EMPTY_RESPONSE_DATA
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNil(configuration)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testConfigurationWithEmptyResponse() {
        apiClient.configureMerchantAuthKey("1sdfghhjkjhgvbc")
        
        let manager = HttpRequestManagerMock()
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        apiClient.retrieveConfiguration { (configuration, error) in
            XCTAssertNil(configuration)
            XCTAssertNotNil(error)
            if let error = error {
                XCTAssertNotNil(error.serviceInvocationError)
                if let error = error.serviceInvocationError {
                    XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                }
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func retrieveApplePayTokenWithEmptyParamsAuthKey() {
        let parameters = """
            "Content-Type": "application/json",
             "cache-control": "no_cache"]
            """
        let responseString = """
                "responseMessage": "Merchant auth key can't be empty"
            """
        if let data = parameters.data(using: .utf8) {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                apiClient.configureMerchantAuthKey("")
                let expectedData = responseString.data(using: .utf8)
                let manager = HttpRequestManagerMock()
                manager.data = expectedData
                apiClient.manager = manager
                let expectation = XCTestExpectation(description: "Get apple pay token with empty auth key request")
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let error = error, let invocationError = error.serviceInvocationError {
                        XCTAssertNotNil(invocationError.responseMessage)
                        XCTAssertEqual(invocationError.responseMessage, Constants.ERROR_MERCHANT_CANT_BE_EMPTY)
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    func retrieveApplePayTokenWithEmptyAuthKey() {
        let parameters = """
            "Content-Type": "application/json",
             "cache-control": "no_cache",
             "authKey": "\(applePayMerchantAuthKey)"]
            """
        let responseString = """
                "responseMessage": "Merchant auth key can't be empty"
            """
        if let data = parameters.data(using: .utf8) {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                let expectedData = responseString.data(using: .utf8)
                let manager = HttpRequestManagerMock()
                manager.data = expectedData
                apiClient.configureMerchantAuthKey("")
                apiClient.manager = manager
                let expectation = XCTestExpectation(description: "Get apple pay token with empty auth key request")
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let token = token {
                        XCTAssertEqual("5480036527610742", token.tokenId)
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    
    func testProcessGetApplePayInvalidRequest() {
        let responseString = """
            {
                "result": "A"
            }
            """
        let expectedData = responseString.data(using: .utf8)
        apiClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        manager.data = expectedData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get applePay token invalid request")
        if let data = applePayBodyParams.data(using: .utf8)
        {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            {
                
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let error = error {
                        XCTAssertNotNil(error.serviceInvocationError)
                        if let error = error.serviceInvocationError {
                            XCTAssertEqual(error.responseMessage, Constants.ERROR_CANT_PARSE_TOKEN)
                        }
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    func testProcessGetApplePayTokenError() {
        apiClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        let errorMessage = "Error"
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        if let data = applePayBodyParams.data(using: .utf8)
        {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            {
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let error = error {
                        XCTAssertNotNil(error.serviceInvocationError)
                        if let error = error.serviceInvocationError {
                            XCTAssertEqual(error.responseMessage, errorMessage)
                        }
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    func testProcessGetApplePayTokenWithEmptyAuthKey() {
        apiClient.configureMerchantAuthKey("")
        let manager = HttpRequestManagerMock()
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        if let data = applePayBodyParams.data(using: .utf8)
        {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            {
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let error = error {
                        if error.errorDescription != nil {
                            XCTAssertNotNil(error.errorDescription)
                            XCTAssertEqual(error.errorDescription, Constants.ERROR_MERCHANT_CANT_BE_EMPTY)
                        } else if let invocationError = error.serviceInvocationError {
                            XCTAssertNotNil(invocationError.responseMessage)
                            XCTAssertEqual(invocationError.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                        }
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    func testProcessGetApplePayTokenEmptyResponse() {
        apiClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let manager = HttpRequestManagerMock()
        let errorMessage = Constants.ERROR_EMPTY_RESPONSE_DATA
        manager.error = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        if let data = applePayBodyParams.data(using: .utf8)
        {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            {
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let error = error {
                        XCTAssertNotNil(error.serviceInvocationError)
                        if let error = error.serviceInvocationError {
                            XCTAssertEqual(error.responseMessage, Constants.ERROR_EMPTY_RESPONSE_DATA)
                        }
                    }
                    expectation.fulfill()
                }
                wait(for: [expectation], timeout: 5)
            }
        }
    }
    
    func testProcessGetApplePayTokenErrorResponse() {
        let responseString = """
            {
                "result": "D",
                "responseCode":"EP0055",
                "responseMessage": "Display result to user and request corrected Date. Note: Requires a new Auth Key"
            }
            """
        apiClient.configureMerchantAuthKey(applePayMerchantAuthKey)
        let errorData = responseString.data(using: .utf8)
        let manager = HttpRequestManagerMock()
        manager.responseType = ResponseType.ERROR(code: 500)
        manager.data = errorData
        apiClient.manager = manager
        let expectation = XCTestExpectation(description: "Get apple pay token error")
        if let data = applePayBodyParams.data(using: .utf8)
        {
            if let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            {
                apiClient.retrieveApplePayToken(parameters: dict!) { (token, error) in
                    XCTAssertNil(token)
                    XCTAssertNotNil(error)
                    if let error = error {
                        XCTAssertNotNil(error.serviceInvocationError)
                        if let error = error.serviceInvocationError {
                            XCTAssertEqual(error.responseMessage, "Display result to user and request corrected Date. Note: Requires a new Auth Key")
                        }
                    }
                    expectation.fulfill()
                }
            }
            wait(for: [expectation], timeout: 5)
        }
    }
    
    func testPerformanceExample() {
        self.measure {
        }
    }
}
