//
//  EPCardValidatorTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import XCTest
@testable import EdgePaySDK

class EPCardValidatorTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testValidator() {
        var card = EPCard(number: "4111111111111111", expirationMonth: "02", expirationYear: "1225", cvv: nil)
        XCTAssertEqual(0, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "02", expirationYear: "1225", cvv: "12345")
        XCTAssertEqual(1, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "02", expirationYear: "1", cvv: nil)
        XCTAssertEqual(1, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "02", expirationYear: "1", cvv: "12345")
        XCTAssertEqual(2, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "16", expirationYear: "123", cvv: "123")
        XCTAssertEqual(2, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "16", expirationYear: "2024", cvv: nil)
        XCTAssertEqual(1, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "16", expirationYear: "2024", cvv: "")
        XCTAssertEqual(1, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "16", expirationYear: "2024", cvv: "12345")
        XCTAssertEqual(2, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "4111111111111111", expirationMonth: "16", expirationYear: "1", cvv: "12345")
        XCTAssertEqual(3, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "1234123412341", expirationMonth: "02", expirationYear: "1225", cvv: nil)
        XCTAssertEqual(1, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "1234123412341", expirationMonth: "02", expirationYear: "1225", cvv: "12345")
        XCTAssertEqual(2, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "1234123412341", expirationMonth: "02", expirationYear: "1000", cvv: "12345")
        XCTAssertEqual(2, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "1234123412341", expirationMonth: "16", expirationYear: "1000", cvv: "12345")
        XCTAssertEqual(3, EPCardValidator.validate(card: card).count)
        
        card = EPCard(number: "23423adfads", expirationMonth: "16", expirationYear: "1000", cvv: "12345")
        XCTAssertEqual(3, EPCardValidator.validate(card: card).count)
    }

    func testPerformanceExample() {
        self.measure {
        }
    }

}
