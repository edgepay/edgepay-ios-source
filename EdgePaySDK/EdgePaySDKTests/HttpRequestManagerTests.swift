//
//  HttpRequestManagerTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import XCTest
@testable import EdgePaySDK

class HttpRequestManagerTests: XCTestCase {
    
    private var httpRequestManager: HttpRequestManager!
    private let session = URLSessionMock()
    private let GET_TOKEN_REQUEST_TYPE = "POST"
    
    override func setUp() {
        super.setUp()
        
        httpRequestManager = HttpRequestManager(session: session)
    }

    override func tearDown() {
        super.tearDown()
    }

    func testWebServiceCallWithInvalidUrl() {
        let expectation = XCTestExpectation(description: "Get token request")
        httpRequestManager.performWebServiceCall(url: "#$%", httpMethod: .post, parameters: [:], headers: [:]) { (data, response, error) in
            XCTAssertNotNil(error)
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        XCTAssertNil(session.lastURL)
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testWebServiceCallWithValidUrl() {
        guard let url = URL(string: "https://mockurl") else {
            fatalError("URL can't be empty")
        }
        let expectation = XCTestExpectation(description: "Get token request")
        httpRequestManager.performWebServiceCall(url: "https://mockurl", httpMethod: .post, parameters: [:], headers: ["header":"value"]) { (data, response, error) in
            expectation.fulfill()
        }
        XCTAssert(session.lastURL == url)
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testResumeCalled() {
        let dataTask = URLSessionDataTaskMock()
        session.dataTask = dataTask
        let expectation = XCTestExpectation(description: "Resume called")
        httpRequestManager.performWebServiceCall(url: "https://mockurl", httpMethod: .post, parameters: [:], headers: [:]) { (data, response, error) in
            expectation.fulfill()
        }
        XCTAssert(dataTask.resumeWasCalled)
        wait(for: [expectation], timeout: 10.0)
    }

    func testWithReturnData() {
        let expectedData = "{}".data(using: .utf8)
        session.data = expectedData
        var actualData: Data?
        let expectation = XCTestExpectation(description: "Resume called")
        httpRequestManager.performWebServiceCall(url: "https://mockurl", httpMethod: .post, parameters: [:], headers: [:]) { (data, response, error) in
            actualData = data
            expectation.fulfill()
        }
        XCTAssertNotNil(actualData)
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testWithReturnError() {
        let errorMessage = "Error message"
        let expectedError = EdgePaySDKError.serverSideError(code: 500, message: errorMessage)
        session.error = expectedError
        var actualError: Error?
        let expectation = XCTestExpectation(description: "Resume called")
        httpRequestManager.performWebServiceCall(url: "https://mockurl", httpMethod: .post, parameters: [:], headers: [:]) { (data, response, error) in
            actualError = error
            expectation.fulfill()
        }
        XCTAssertNotNil(actualError)
        if let error = actualError, case EdgePaySDKError.serverSideError(let code, let message) = error {
            XCTAssert(code == 500)
            XCTAssert(message == errorMessage)
        }
        wait(for: [expectation], timeout: 10.0)
    }

    func testPerformanceExample() {
        self.measure {
        }
    }
}
