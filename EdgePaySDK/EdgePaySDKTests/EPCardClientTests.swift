//
//  EPCardClientTests.swift
//  EdgePaySDKTests
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import XCTest
@testable import EdgePaySDK

class EPCardClientTests: XCTestCase {

    private var testApiClient = EPAPIClientMock(testMode: true)
    private var testCardClient: EPCardClient!
    
    private var prodApiClient = EPAPIClientMock(testMode: false)
    private var prodCardClient: EPCardClient!

    
    override func setUp() {
        super.setUp()
        
        testCardClient = EPCardClient(apiClient: testApiClient)
        prodCardClient = EPCardClient(apiClient: prodApiClient)
    }

    override func tearDown() {
        super.tearDown()
    }

    func testGetValidCardToken() {
        let card = EPCard(number: "4111111111111111", expirationMonth: "02", expirationYear: "2021", cvv: nil)
        let expectation = XCTestExpectation(description: "Get token with valid card data")
        testCardClient.getToken(card: card) { (token, error) in
            XCTAssertNil(error)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }
    
    func testGetInvalidCardToken() {
        let card = EPCard(number: "123412341", expirationMonth: "02", expirationYear: "12", cvv: "12")
        let expectation = XCTestExpectation(description: "Get token with invalid card data")
        prodCardClient.getToken(card: card) { (token, error) in
            XCTAssertNil(token)
            XCTAssertNotNil(error)
            guard let error = error else { return }
            XCTAssertNotNil(error.validationErrors)
            guard let validationErrors = error.validationErrors else { return }
            XCTAssertEqual(validationErrors.count, 2)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5)
    }

    func testPerformanceExample() {
        self.measure {
        }
    }

}
