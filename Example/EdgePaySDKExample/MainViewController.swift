//
//  MainViewController.swift
//  EdgePaySDKExample
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import UIKit
import EdgePaySDK
import MBProgressHUD
import PassKit

class MainViewController: UIViewController, PKPaymentAuthorizationViewControllerDelegate {
    
    @IBOutlet private var stackView: UIStackView!
    
    private var edgePayClient = EPAPIClient(testMode: true)
    private var appleClient: EPApplePayClient?
    private var authHelper = AuthKeyHelper()
    private var progress: MBProgressHUD?
    
    private var merchantKey: String?
    private var merchantId: String?
    private var terminalID: String?
    private var appleClientToken: (status: Bool, tokenId: String) = (false,"")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createApplePayButton()
    }
    
    private func createApplePayButton() {
        progress?.hide(animated: true)
        progress = MBProgressHUD.showAdded(to: self.view, animated: true)
        let result = PaymentHandler.applePayStatus()
        var button: UIButton?
        
        if result.canMakePayments {
            button = PKPaymentButton(paymentButtonType: .buy, paymentButtonStyle: .black)
            button?.addTarget(self, action: #selector(self.getAppleClient(_:)), for: .touchUpInside)
        } else if result.canSetupCards {
            button = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .black)
            button?.addTarget(self, action: #selector(self.setupPressed), for: .touchUpInside)
        }
        
        if let applePayButton = button {
            self.stackView.addArrangedSubview(applePayButton)
        }
        progress?.hide(animated: true)
    }
    
    
    @objc func getAppleClient(_ sender: Any) {
        progress?.hide(animated: true)
        progress = MBProgressHUD.showAdded(to: self.view, animated: true)
        authHelper.getMerchantAuthKey { (merchantAuthKey, error) in
            if let error = error {
                DispatchQueue.main.async {[weak self] in
                    self?.progress?.hide(animated: true)
                    self?.showAlert(with: "Error", message: error.localizedDescription)
                    return
                }
            }
            guard let merchantAuthKey = merchantAuthKey else {
                DispatchQueue.main.async {[weak self] in
                    self?.progress?.hide(animated: true)
                    self?.showAlert(with: "Error", message: "Merchant auth key is empty")
                }
                return
            }
            self.edgePayClient.configureMerchantAuthKey(merchantAuthKey)
            self.appleClient = EPApplePayClient(apiClient: self.edgePayClient)
            self.getPKPaymentRequest(completion: { (paymentRequest, error) in
                if let error = error {
                    DispatchQueue.main.async {[weak self] in
                        self?.progress?.hide(animated: true)
                        if let serviceError = error.serviceInvocationError, let message = serviceError.responseMessage {
                            self?.showAlert(with: "Error", message: message)
                        }
                        return
                    }
                }
                guard let payment = paymentRequest else {
                    return
                }
                
                payment.merchantCapabilities = .capability3DS
                payment.paymentSummaryItems = [
                    PKPaymentSummaryItem(label: "GET T-Shirt", amount: NSDecimalNumber(string: "25.90")),
                    PKPaymentSummaryItem(label: "GET eSHop", amount: NSDecimalNumber(string: "25.90")),
                ]
                
                let paymentAuthorizationVC = PKPaymentAuthorizationViewController(paymentRequest: payment)
                paymentAuthorizationVC?.delegate = self
                if PKPaymentAuthorizationViewController.canMakePayments() {
                    guard let vc = paymentAuthorizationVC else { return }
                    DispatchQueue.main.async {[weak self] in
                        self?.present(vc, animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {[weak self] in
                        self?.progress?.hide(animated: true)
                        self?.showAlert(with: "Error", message: "Please setup Apple Pay before trying to purchase credits")
                    }
                }
            })
        }
    }
    
    @objc func setupPressed(sender: AnyObject) {
        let passLibrary = PKPassLibrary()
        passLibrary.openPaymentSetup()
    }
    
    
    private func getPKPaymentRequest(completion: @escaping ((PKPaymentRequest?, EPError?) -> Void)) {
        self.appleClient?.getPaymentRequest(completion: { (paymentRequest, error) in
            guard let paymentRequest = paymentRequest else {
                completion(nil, error)
                return
            }
            completion(paymentRequest, nil)
        })
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        if appleClientToken.status {
            controller.dismiss(animated: true) { [weak self] in
                guard let self = self else { return }
                self.progress?.hide(animated: true)
                self.showAlert(with: "Success", message: self.appleClientToken.tokenId)
                self.appleClientToken = (false, "")
            }
        } else {
            self.progress?.hide(animated: true)
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController,
                                            didAuthorizePayment payment: PKPayment,
                                            completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        guard let appleClient = self.appleClient else {
            completion(.failure)
            return
        }
        appleClient.getToken(payment: payment, completion: { [weak self] (token, error) in
            guard let token = token else {
                completion(.failure)
                return
            }
            self?.appleClientToken = (true, token.tokenId)
            completion(.success)
            return
        })
    }
    
}
