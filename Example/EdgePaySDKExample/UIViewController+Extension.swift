//
//  UIViewController+Extension.swift
//  EdgePaySDKExample
//
//  Copyright © 2021 EdgePay. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(with title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(with title: String, message: String, completion: @escaping(()-> Void)) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            completion()
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}
