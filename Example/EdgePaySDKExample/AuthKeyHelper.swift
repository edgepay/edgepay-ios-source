//
//  AuthKeyHelper.swift
//  EdgePaySDKExample
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

enum EdgePaySDKExampleError: Error {
    case serverSideError(code: Int, message: String)
}

enum EdgePaySDKExamplePaymentType {
    case EdgePay, ApplePay
    
    var url: String {
        switch self {
        case .EdgePay:
            return "https://api.edgepay-uat.com/generateAuthKey"
        case .ApplePay:
            return "https://2nu4djjmal.execute-api.us-west-2.amazonaws.com/api/authkey"
        }
    }
}

final class AuthKeyHelper {
    private static var externalReferenceId: Int64 = 1000000000000000
    private static var merchantExternalReferenceId: Int64 = 1231231231231231
    
    func getAuthKey(completion: @escaping ((String?, Error?) -> Void)) {
        let parameters = getRequestParameters(for: .EdgePay)
        let headers = getRequestHeaders(for: .EdgePay)
        AuthKeyHelper.externalReferenceId += 1
        processGetAuthKeyRequest(parameters, headers, for: .EdgePay, completion: completion)
    }
    
    func getMerchantAuthKey(completion: @escaping ((String?, Error?) -> Void)) {
        let parameters = getRequestParameters(for: .ApplePay)
        let headers = getRequestHeaders(for: .ApplePay)
        processGetAuthKeyRequest(parameters, headers, for: .ApplePay, completion: completion)
    }
    
    private func getRequestParameters(for type: EdgePaySDKExamplePaymentType) -> [String: String] {
        var parameters = [String: String]()
        switch type {
        case .EdgePay:
            parameters = ["merchantID": "6320340123456901",
                          "terminalID": "88800000282601"]
        case .ApplePay:
            parameters = ["merchantID": "9330585160",
                          "terminalID": "9330585160"]
        }
        
        return parameters
    }
    
    private func getRequestHeaders(for type: EdgePaySDKExamplePaymentType) -> [String: String] {
        var headers = [String: String]()
        switch type {
        case .EdgePay:
            headers = ["Content-Type": "application/json",
                       "cache-control": "no_cache",
                       "externalReferenceID": "\(AuthKeyHelper.externalReferenceId)",
                       "merchantKey": "C8E05D70D3C8D3D675EAA2FE86674FEA3BCE1AFBECA4322C",
                       "transactionDate": "2019-02-25T00:02:00"]
        case .ApplePay:
            headers = ["Content-Type": "application/json",
                       "cache-control": "no_cache",
                       "externalReferenceID": "\(AuthKeyHelper.merchantExternalReferenceId)",
                       "merchantKey": "B9153D54C7CE461594616E169DCB45E0C1CB859DE61C7AF2"]
        }
        
        return headers
    }
    
    private func processGetAuthKeyRequest(_ parameters: [String: String], _ headers: [String: String], for type: EdgePaySDKExamplePaymentType, completion: @escaping ((String?, Error?) -> Void)) {
        APIManager.instance.performWebServiceCall(url: type.url,
                                                  httpMethod: "POST",
                                                  parameters: parameters,
                                                  headers: headers) { (data, response, error) in
                                                    if let error = error {
                                                        completion(nil, error)
                                                        return
                                                    }
                                                    guard let responseData = data else {
                                                        let message = "Error: did not receive data"
                                                        completion(nil, EdgePaySDKExampleError.serverSideError(code: NSURLErrorDataNotAllowed, message: message))
                                                        return
                                                    }
                                                    var authKey: String?
                                                    let data = try? JSONSerialization.jsonObject(with: responseData, options: [])
                                                    guard let receivedData = data as? [String: Any] else {
                                                        let message = "Could not get JSON from responseData as dictionary"
                                                        completion(nil, EdgePaySDKExampleError.serverSideError(code: NSURLErrorDataNotAllowed, message: message))
                                                        return
                                                    }
                                                    switch type {
                                                    case .EdgePay:
                                                        authKey = receivedData["pivotAuthKey"] as? String
                                                    case .ApplePay:
                                                        guard let key = receivedData["authKey"] as? String else {
                                                            guard let message = receivedData["responseMessage"] as? String else {
                                                                completion(nil, EdgePaySDKExampleError.serverSideError(code: NSURLErrorDataNotAllowed, message: "Something went wrong" ))
                                                                return
                                                            }
                                                            completion(nil, EdgePaySDKExampleError.serverSideError(code: NSURLErrorDataNotAllowed, message: message ))
                                                            return
                                                        }
                                                        authKey = key
                                                    }
                                                    
                                                    completion(authKey, nil)
        }
        
    }
}

extension EdgePaySDKExampleError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .serverSideError(_, let message): return message
        }
    }
}
