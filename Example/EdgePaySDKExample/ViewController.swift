//
//  ViewController.swift
//  EdgePaySDKExample
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import UIKit
import EdgePaySDK
import MBProgressHUD

class ViewController: UIViewController {

    @IBOutlet weak var editCardNumber: UITextField!
    @IBOutlet weak var editMonth: UITextField!
    @IBOutlet weak var editYear: UITextField!
    @IBOutlet weak var editCvv: UITextField!
    
    private var edgePayClient = EPAPIClient(testMode: true)
    private var cardClient: EPCardClient?
    private var authHelper = AuthKeyHelper()
    private var progress: MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
        editCardNumber.text = "4111111111111111"
        editMonth.text = "2"
        editYear.text = "2023"
        editCvv.text = "123"
        #endif
        
        self.initCardClient()
    }

    private func initCardClient() {
        cardClient = EPCardClient(apiClient: edgePayClient)
    }
    
    private func getToken(completion: @escaping ((String?, String?) -> Void)) {
        guard let card = prepareEpCard() else {
            completion(nil, "Invalid card data!")
            return
        }
        cardClient?.getToken(card: card, completion: { (token, error) in
            DispatchQueue.main.async { [weak self] in
                var errorMessage: String?
                if let error = error {
                    errorMessage = self?.getMessage(error: error)
                }
                completion(token?.tokenId, errorMessage)
            }
        })
    }
    
    private func getMessage(error: EPError) -> String {
        var message: String = ""
        if error.errorType == EPErrorType.VALIDATION_FAILED {
            guard let validationErrors = error.validationErrors else {
                return message
            }
            for error in validationErrors {
                message.append(error.message)
                if message.count <= 0 {
                    continue
                }
                message.append(" \n")
            }
        } else {
            let serviceInvocationError = error.serviceInvocationError
            let errorMessage = serviceInvocationError?.responseMessage
            message = errorMessage ?? ""
        }
        return message
    }
    
    private func prepareEpCard() -> EPCard? {
        guard let number = editCardNumber.text, let month = editMonth.text, let year = editYear.text else {
            return nil
        }
        return EPCard(number: number, expirationMonth: month, expirationYear: year, cvv: editCvv.text)
        
    }

    @IBAction func getToken(_ sender: Any) {
        progress?.hide(animated: true)
        progress = MBProgressHUD.showAdded(to: self.view, animated: true)
        authHelper.getAuthKey { (merchantAuthKey, error) in
            DispatchQueue.main.async { [weak self] in
                if let error = error {
                    self?.progress?.hide(animated: true)
                    self?.showAlert(with: "Error", message: error.localizedDescription)
                    return
                }
                guard let merchantAuthKey = merchantAuthKey else {
                    self?.progress?.hide(animated: true)
                    self?.showAlert(with: "Error", message: "Merchant auth key is empty")
                    return
                }
                self?.edgePayClient.configureMerchantAuthKey(merchantAuthKey)
                self?.getToken(completion: { (token, errorMessage) in
                    self?.progress?.hide(animated: true)
                    var title = "Success"
                    var message = token ?? "Something went wrong"
                    if let errorMessage = errorMessage {
                        title = "Error"
                        message = errorMessage
                    }
                    self?.showAlert(with: title, message: message)
                })
            }
        }
    }
    
}

extension ViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if textField == editMonth {
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }
        if textField == editYear {
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }
        if textField == editCvv {
            let newLength = text.count + string.count - range.length
            return newLength <= 3
        }
        return true
        
    }
}

