//
//  APIManager.swift
//  EdgePaySDKExample
//
//  Copyright © 2019 EdgePay. All rights reserved.
//

import Foundation

class APIManager {

    static let instance = APIManager()
    
    private init() {}
    
    func performWebServiceCall(url: String, httpMethod: String, parameters: [String: Any],
                               headers: [String: String], completion: @escaping ((Data?, URLResponse?, Error?) -> Void)) {
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
        do {
            let bodyData = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
            request.httpBody = bodyData
        } catch let error {
            completion(nil, nil, error)
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            completion(data, response, error)
        }
        task.resume()
    }
}
