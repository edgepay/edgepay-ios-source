# EdgePay SDK 

The EdgePay iOS SDKs allow merchants to tokenize credit card details within their mobile applications without passing them through their servers so they can remain PCI-DSS compliant.
The SDKs will leverage the same API endpoints that EdgePay uses to generate tokens and it will return the same validation and service errors.
The SDKs will be designed to be simple and easy for developers to use, and flexible and loosely coupled for the GET team to expand and add new features as needed. 


## Getting Started
The project contains 2 modules: the library module and the example module.  Supported since iOS 9.0 and higher.
The SDKs will reside in the merchants' mobile application and will provide 5 public functions: Initialize, ConfigureMerchantAuthKey, EPCard, GetPaymentRequest, and GetToken.
The tokenization process will use EdgePay's API through a secure and encrypted channel using the TLS protocol with the appropriate settings

## Requirements

- iOS 9.0+
- Xcode 10.1+
- Swift 4.2+

## Building project ![build](https://build.appcenter.ms/v0.1/apps/8ca59149-1c12-4d7b-8556-1b880b166751/branches/master/badge) 

[CI](https://appcenter.ms) is configured to automatically build the project after push into a master branch. 

## Running the tests ![codecov](https://codecov.io/bb/edgepay/edgepay-ios-source/branch/master/graph/badge.svg)

SDK code is covered by unit tests.  
Tests report is sending on [Codecov](https://codecov.io/) ([last report](https://codecov.io/bb/edgepay/edgepay-ios-source/)).   
[Codecov](https://codecov.io/) provides highly integrated tools to group, merge, archive, and compare coverage reports.
The departure occurs at the time of the project assembly on [CI](https://appcenter.ms).  

CodeCov has an issue when is used with AppCenter. AppCenter moves test operation output data from the Derived Data directory to another one.
So CodeCov cannot find the code coverage report. To solve this problem, the initial CodeCov script was fetched locally and modified (directory replaced with another one where code coverage report moved, see here https://bitbucket.org/edgepay/edgepay-ios-source/commits/a9ed6ab9c1501cc3edfd07533fe0e92716a87337?at=master). It was added to this repository as `codeCovScript.sh`.
Also another `appcenter-post-build.sh` was added to run CodeCov script. AppCenter starts it at the end of the build.

## Integrate SDK

See information in this [integration document](INTEGRATION.md)


## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## Publishing

To publish a new version you should:

1. Clone repository from  from https://bitbucket.org/edgepay/edgepay-ios-sdk/src/master/
2. Update the version in podspec beforehand.
3. Commit, push code to library repository.
4. Create a new tag with the current code, make sure it's the same tag as the version field value in podspec.
    `git tag <#INSERT TAG NUMBER HERE#>`
5. Push the tag to the repository.
    `git push origin <#INSERT TAG NUMBER HERE#>`
6. Update spec.version into EdgePaySDK.podspec on edgepay-ios-source
7. Call pod spec lint to check, that everything is configured correctly.
    `pod spec lint EdgePaySDK.podspec`
8. Call pod repo push to update it on repo master list.
    `pod repo push EdgePay-iOS-SDK EdgePaySDK.podspec --allow-warnings`
9. In case of errors similar to "Unable to find the 'Edge-Pay-iOS-SDK' repo, it means that you did not clone it.
    To clone the repo you need to  use 'pod repo add'." 
    `pod repo add EdgePay-iOS-SDK https://bitbucket.org/edgepay/edgepay-ios-source.git`.
    Then try again point 8.
10. Now you can add SDK to the project using the value of the added tag. 
